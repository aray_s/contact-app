package com.`as`.contactapp.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.`as`.contactapp.R
import com.`as`.contactapp.databinding.ItemBinding
import com.`as`.contactapp.model.Contact
import com.bumptech.glide.Glide

class RecyclerViewAdapter(private val context: Context, contacts: List<Contact>) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    private var contactList: List<Contact> = contacts

    private val clickListener: (Contact) -> Unit = { contact ->
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:${contact.phoneNumber}")
        }
        context.startActivity(intent)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val binding = ItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contactList[position]
        holder.bind(contact, clickListener)
    }

    override fun getItemCount() = contactList.size

    inner class ViewHolder(private val binding: ItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(contact: Contact, clickListener: (Contact) -> Unit) {
            binding.contactName.text = contact.name
            binding.contactNumber.text = contact.phoneNumber

            val photoUri = contact.imageUri
            if (photoUri != null) {
                Glide.with(binding.root.context)
                    .load(photoUri)
                    .into(binding.contactImage)
            } else {
                binding.contactImage.setImageResource(R.drawable.ic_contact)
            }

            binding.root.setOnClickListener { clickListener(contact) }
        }
    }
}
