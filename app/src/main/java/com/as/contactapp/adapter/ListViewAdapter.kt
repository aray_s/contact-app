package com.`as`.contactapp.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.recyclerview.widget.RecyclerView
import com.`as`.contactapp.R
import com.bumptech.glide.Glide
import com.`as`.contactapp.databinding.ItemBinding
import com.`as`.contactapp.model.Contact

class ListViewAdapter(private val context: Context, private var contacts: List<Contact>) :
    BaseAdapter() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    private val clickListener: (Contact) -> Unit = { contact ->
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:${contact.phoneNumber}")
        }
        context.startActivity(intent)
    }

    override fun getCount() = contacts.size

    override fun getItem(p0: Int) = contacts[p0]

    override fun getItemId(p0: Int) = p0.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        val holder: ViewHolder
        if (view == null) {
            val binding = ItemBinding.inflate(inflater, parent, false)
            view = binding.root
            holder = ViewHolder(binding)
            view.tag = holder
        } else holder = view.tag as ViewHolder
        val contact = contacts[position]
        holder.bind(contact, clickListener)
        return view
    }

    inner class ViewHolder(private val binding: ItemBinding) {
        fun bind(contact: Contact, clickListener: (Contact) -> Unit) {
            binding.contactName.text = contact.name
            binding.contactNumber.text = contact.phoneNumber

            val photoUri = contact.imageUri
            if (photoUri != null) {
                Glide.with(binding.root.context)
                    .load(photoUri)
                    .into(binding.contactImage)
            } else binding.contactImage.setImageResource(R.drawable.ic_contact)

            binding.root.setOnClickListener { clickListener(contact) }
        }
    }
}