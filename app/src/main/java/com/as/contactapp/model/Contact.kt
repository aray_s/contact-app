package com.`as`.contactapp.model

import android.net.Uri
data class Contact(
    val name: String,
    val phoneNumber: String,
    val imageUri: Uri?
)
