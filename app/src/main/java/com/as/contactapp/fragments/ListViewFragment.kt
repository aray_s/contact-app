package com.`as`.contactapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.`as`.contactapp.adapter.ListViewAdapter
import com.`as`.contactapp.databinding.FragmentListViewBinding

class ListViewFragment : BasicFragment() {

    private lateinit var adapter: ListViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val binding = FragmentListViewBinding.inflate(inflater, container, false)
        adapter = ListViewAdapter(requireContext(), contactsList)
        binding.listView.adapter = adapter
        requestPermission()
        return binding.root
    }

    override fun loadContacts() {
        readContacts()
        adapter.notifyDataSetChanged()
        cursor?.close()
    }
}

