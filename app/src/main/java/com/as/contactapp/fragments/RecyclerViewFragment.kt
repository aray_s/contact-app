package com.`as`.contactapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.`as`.contactapp.adapter.RecyclerViewAdapter
import com.`as`.contactapp.databinding.FragmentRecyclerViewBinding

class RecyclerViewFragment : BasicFragment() {

    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val binding = FragmentRecyclerViewBinding.inflate(inflater, container, false)
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter = RecyclerViewAdapter(requireContext(), contactsList)
        binding.recyclerView.adapter = adapter
        requestPermission()
        return binding.root
    }

    override fun loadContacts() {
        readContacts()
        adapter.notifyDataSetChanged()
        cursor?.close()
    }
}