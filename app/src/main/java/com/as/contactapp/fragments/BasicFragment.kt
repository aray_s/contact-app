package com.`as`.contactapp.fragments

import android.content.ContentUris
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.ContactsContract
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.`as`.contactapp.model.Contact
import java.io.File
import java.io.FileOutputStream

abstract class BasicFragment : Fragment() {

    val contactsList = ArrayList<Contact>()
    var cursor: Cursor? = null

    abstract fun loadContacts()

    fun requestPermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(android.Manifest.permission.READ_CONTACTS),
                1
            )
        } else {
            loadContacts()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadContacts()
                } else {
                    Toast.makeText(activity, "You denied the permission", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun readContacts() {
        cursor = (requireContext()).contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null, null, null, null
        )?.apply {
            while (moveToNext()) {
                val displayName =
                    getString(getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                val number =
                    getString(getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER))
                val image =
                    getContactPhotoUri(getLong(getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)))
                contactsList.add(Contact(displayName, number, image))
            }
        }
    }

    private fun getContactPhotoUri(contactId: Long): Uri? {
        val contentResolver = context?.contentResolver
        val photoUri = ContentUris.withAppendedId(
            ContactsContract.Contacts.CONTENT_URI,
            contactId
        )
        val inputStream = ContactsContract.Contacts.openContactPhotoInputStream(
            contentResolver,
            photoUri
        )
        return if (inputStream != null) {
            val bitmap = BitmapFactory.decodeStream(inputStream)
            inputStream.close()
            val file = File(context?.cacheDir, "contact_photo_${contactId}.jpg")
            val fileOutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)
            fileOutputStream.close()
            Uri.fromFile(file)
        } else {
            null
        }
    }
}